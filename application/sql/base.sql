CREATE TABLE IF NOT EXISTS `cismas`.`news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  `content` TEXT,
  `file_destaque` varchar(255) NULL,
  `active` int(1) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `modified_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `cismas`.`image_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_new` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `active` int(1) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `modified_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;