<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class New_model extends MY_Model{
    public $table = 'news';
    public $primary_key = 'id';

    public function __construct()
    {
        $this->has_one['user'] = array('Users_m','id', 'id_user');

        parent::__construct();
    }
}