<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class File_model extends MY_Model{
    public $table = 'files_uploaded';
    public $primary_key = 'id';
}