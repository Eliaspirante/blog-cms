<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    const group_edital = 'edital';
    const group_admin = 'admin';

    function __construct()
    {
        parent::__construct();

        if(!$this->ion_auth->logged_in())
            redirect('/welcome', 'refresh');
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect("/admin/login", 'refresh');
        }

        $this->load->view('admin/dashboard');
    }

    public function news(){
        if($this->ion_auth->in_group(self::group_edital)){
            redirect("/admin/", 'refresh');
        }

        $this->load->model("New_model");

        $data['news'] = $this->New_model->with_user()->get_all();

        $this->load->view('admin/news/list', $data);
    }

    public function addNew(){
        if($this->ion_auth->in_group(self::group_edital)){
            redirect("/admin/", 'refresh');
        }

        if($_POST){
            $conteudo = $_POST['content'];
            $title = $_POST['title'];
            $description = $_POST['description'];
            $file_destaque = "";
            $active = 0;

            if(array_key_exists("active", $_POST)){
                $active = 1;
            }

            if(array_key_exists("files", $_POST)){

            }

            if(array_key_exists("file_destaque", $_FILES)){
                $file_destaque = $_FILES['file_destaque']['name'];

                if (!move_uploaded_file(
                    $_FILES['file_destaque']['tmp_name'],'./articles/'.$_FILES['file_destaque']['name']
                )) {
                    throw new RuntimeException('Erro ao salvar arquivo.');
                }
            }

            $array_data = array(
                    'content' => $conteudo,
                    'title' => $title,
                    'description' => $description,
                    'file_destaque' => $file_destaque,
                    'id_user' => $this->ion_auth->user()->row()->id,
                    'active' => $active
                    );

            $this->load->model("New_model");

            $id = $this->New_model->insert($array_data);


//            'content' => string '<p><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATYAAACnCAIAAAAUk+esAAAAA3NCSVQICAjb4U/gAAAAGXRFWHRTb2Z0d2FyZQBnbm9tZS1zY3JlZW5zaG907wO/PgAAHAtJREFUeJzt3X1w2+adJ/Afd3aKJzu3BW52x8DONSI6l5iYW9ekndjCJKcQdZsj1E2G1DWxmGlSMb1rRG/TiGnTiFmfXdVpKjqTxrTjXTF9GdF5GTEvd6QmHhNeXyMmbiquE5eU1xnB9uUIOZkISn0lWN+Ej/aP4/1BvVCUJdOK42I2v8/oD4nP8wAPIH714KHw4qjVaoAQsqs/+WN3ACG0GowoQraGEUXI1jCiCNkaRhQhW8OIImRrGFGEbA0jipCtYUQRsjWMKEK2hhFFyNYwogjZGkYUIVvDiCJkaxhRhGwNI4qQrV06ojQX5ByLiKrRZVWKEdEhRorLCi7FSChiULOaX7a0oKgkjEu3M'... (length=9937)
//  'title' => string 'asdasd' (length=6)
//  'description' => string 'qweqweqwe' (length=9)
//  'active' => string 'on' (length=2)
//  'files' => string 'brasopolis_ceps.png' (length=19)

            redirect("/admin/news/", 'refresh');
        }

        $this->load->view('admin/news/new');
    }

    public function editNew($id){
        if($this->ion_auth->in_group(self::group_edital)){
            redirect("/admin/", 'refresh');
        }

        if($id == null)
            redirect('/dashboard/news', 'refresh');

        $this->load->model("New_model");

        $data['new'] = $this->New_model->get($id);

        if($_POST){
            $conteudo = $_POST['content'];
            $title = $_POST['title'];
            $description = $_POST['description'];
            $file_destaque = "";
            $active = 0;

            if(array_key_exists("active", $_POST)){
                $active = 1;
            }

            if(array_key_exists("file_destaque", $_POST)){
                $file_destaque = $_POST['file_destaque'];
            }

            if(array_key_exists("files", $_POST)){

            }

            $array_data = array(
                'content' => $conteudo,
                'title' => $title,
                'description' => $description,
                'file_destaque' => $file_destaque,
                'id_user' => $this->ion_auth->user()->row()->id,
                'active' => $active
            );

            $this->load->model("New_model");

            $update = $this->New_model->update($array_data, $id);

            if($update){

            }else{

            }

            $data['new'] = $this->New_model->get($id);
        }

        $this->load->view('admin/news/edit', $data);
    }

    public function deleteNew(){
        if($_POST){
            $this->load->model("New_model");
            $delete = $this->New_model->delete($_POST['article']);

            $msg = "";

            if(!$delete){
                $msg = array("nok" => "Algum erro ocorreu durante a exclusão da notícia.");
            }else{
                $msg = array("ok" => "Notícia excluída com sucesso!");
            }

            echo json_encode($msg);
        }
    }

    public function showNew(){
        if($_POST){

        }

        $this->load->view('admin/news/new');
    }

    /*** EDITAIS E ARQUIVOS ***/

    public function editals(){
        $this->load->model("Edital_model");

        $data['editals'] = $this->Edital_model->with_user()->with_files()->get_all();

        $this->load->view('admin/editals/list', $data);
    }

    public function addEdital(){
        if($_POST){

            $title = $_POST['title'];
            $description = $_POST['description'];
            $active = 0;

            if(array_key_exists("active", $_POST)){
                $active = 1;
            }

            if(array_key_exists("name_file", $_POST)){

                $array_data = array(
                    'title' => $title,
                    'description' => $description,
                    'id_user' => $this->ion_auth->user()->row()->id,
                    'active' => $active
                );

                $this->load->model("Edital_model");
                $this->load->model("File_model");

                $id_edital = $this->Edital_model->insert($array_data);

                for($i = 0; $i < count($_FILES['files']['tmp_name']); $i++){
                    if (!move_uploaded_file(
                        $_FILES['files']['tmp_name'][$i],'./uploads/'.$id_edital.'_'.$_FILES['files']['name'][$i])) {
                        throw new RuntimeException('Erro ao salvar arquivo.');
                    }else{

                        $data_file = array(
                            "id_user" => $this->ion_auth->user()->row()->id,
                            "description" => $_POST['name_file'][$i],
                            "name" => $id_edital.'_'.$_FILES['files']['name'][$i],
                            "active" => 1,
                            "id_edital" => $id_edital,
                        );

                        $this->File_model->insert($data_file);
                    }
                }

//                redirect("/admin/editals/", 'refresh');
            }else{
                $data["message"] = array("nok" => "Erro ao inserir arquivos");
            }
        }

        $this->load->view('admin/editals/new');
    }

    public function editEdital($id){

        if($id == null)
            redirect('/dashboard/editals', 'refresh');

        $this->load->model("Edital_model");
        $this->load->model("File_model");

        $data['edital'] = $this->Edital_model->with_files()->get($id);

        if($_POST){
            $title = $_POST['title'];
            $description = $_POST['description'];
            $active = 0;

            if(array_key_exists("active", $_POST)){
                $active = 1;
            }

            foreach($_POST as $key => $value){
                if(strpos($key, "checkdel") !== false){
                    $id_to_delete = explode("_", $key);

                    $file_to_delete = $this->File_model->get($id_to_delete[1]);

                    if($file_to_delete != false){
                        try {
                            unlink('./uploads' . $file_to_delete->name);
                        }catch(Exception $e){

                        }
                        $this->File_model->delete($id_to_delete[1]);
                    }
                }elseif(strpos($key, "name_file_") !== false){
                    $id_to_update = explode("_", $key);
                    $id_to_update = $id_to_update[2];

                    $this->File_model->update(array("description" => $value),$id_to_update);
                }
            }

            $array_data = array(
                'title' => $title,
                'description' => $description,
                'id_user' => $this->ion_auth->user()->row()->id,
                'active' => $active
            );


            $this->Edital_model->update($array_data, $id);

            if(array_key_exists("new_name_file", $_POST)){
                for($i = 0; $i < count($_FILES['newfiles']['tmp_name']); $i++){
                    if (!move_uploaded_file(
                        $_FILES['newfiles']['tmp_name'][$i],'./uploads/'.$id.'_'.$_FILES['newfiles']['name'][$i])) {
                        throw new RuntimeException('Erro ao salvar arquivo.');
                    }else{

                        $data_file = array(
                            "id_user" => $this->ion_auth->user()->row()->id,
                            "description" => $_POST['new_name_file'][$i],
                            "name" => $id.'_'.$_FILES['newfiles']['name'][$i],
                            "active" => 1,
                            "id_edital" => $id,
                        );

                        $this->File_model->insert($data_file);
                    }
                }

                redirect("/admin/editals/", 'refresh');
            }else{
                $data["message"] = array("nok" => "Erro ao inserir arquivos");
            }

            $data['edital'] = $this->Edital_model->with_files()->get($id);
        }

        $this->load->view('admin/editals/edit', $data);
    }

    public function deleteEdital(){
        if($_POST){
            $this->load->model("Edital_model");
            $this->load->model("File_model");

            $edital = $this->Edital_model->with_files()->get($_POST['article']);

            $delete = false;

            if($edital != false) {
                if(isset($edital->files)) {
                    foreach ($edital->files as $file) {
                        try {
                            unlink('./uploads' . $file->name);
                        }catch(Exception $e){

                        }
                        $this->File_model->delete($file->id);
                    }
                }
                $delete = $this->Edital_model->delete($_POST['article']);
            }

            $msg = "";

            if(!$delete){
                $msg = array("nok" => "Algum erro ocorreu durante a exclusão do edital.");
            }else{
                $msg = array("ok" => "Edital excluído com sucesso!");
            }

            echo json_encode($msg);
        }
    }

    public function showEdital(){
        if($_POST){

        }

        $this->load->view('admin/editals/new');
    }

    public function showDownloadedFiles(){

        $this->load->model("Client_download_model");

        $data["files_downloaded"] = $this->Client_download_model->order_by("added", "DESC")->get_all();

        $this->load->view('admin/editals/list_files_downloaded', $data);
    }
}
