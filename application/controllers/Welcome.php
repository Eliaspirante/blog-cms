<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $this->load->model("New_model");

        $data['news'] = $this->New_model->with_user()->where('active','1')->limit(5)->order_by("id",$order = 'DESC')->get_all();

        $this->load->view('novo_teste', $data);
	}

    public function articles(){
        $this->load->model("New_model");

        $data['news'] = $this->New_model->with_user()->where('active','1')->order_by("id",$order = 'DESC')->get_all();

        $this->load->view('site/articles', $data);
    }

    public function testes(){
        $this->load->view('novo_teste');
    }

    public function testeseditais(){

        $this->load->view('site/editais');
    }

    public function article($id){
        if($id == null)
            redirect('/', 'refresh');

        $this->load->model("New_model");

        $data['new'] = $this->New_model->with_user()->get($id);

        $this->load->view('site/article', $data);
    }

    public function editais(){

        if($_POST){
            // var_dump($_POST);

            $this->load->helper('download');

            if($_POST["firstname"] == "" || $_POST["identification_number"] == "" || $_POST["email"] == ""){
                redirect(base_url().'index.php/welcome/editais', 'refresh');
                return;
            }

            $message = "<html>
                <head>
                      <title>Download do arquivo de edital em " . date("d/m/Y H:i") . "</title>
                </head>
                <body><br><br>
                    Nome completo: " . $_POST["firstname"] . "<br>
                    CPF/CNPJ: " . $_POST["identification_number"] . "<br>
                    E-mail: " . $_POST["email"] . "<br>
                    Arquivo: " . $_POST["file"] . "<br>
                </body>
            </html>";

            // $headers   = array();
            // $headers[] = "MIME-Version: 1.0";
            // $headers[] = "Content-type: text/plain; charset=iso-8859-1";
            // $headers[] = "From: Download Cismas <postmaster@cismas.mg.gov.br>";
            // $headers[] = "Bcc: Diego <diego@cismas.mg.gov.br>";
            // $headers[] = "Reply-To: Diego <diego@cismas.mg.gov.br>";
            // $headers[] = "Subject: Download do arquivo de edital em " . date("d/m/Y H:i");
            // $headers[] = "X-Mailer: PHP/".phpversion();

            $uid = md5(uniqid(time()));

            $header = "From: Download Cismas<postmaster@cismas.mg.gov.br>\r\n";
            $header .= "Reply-To: postmaster@cismas.mg.gov.br\r\n";
            $header .= "MIME-Version: 1.0\r\n";
            $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
            $header .= "This is a multi-part message in MIME format.\r\n";
            $header .= "--".$uid."\r\n";
            $header .= "Content-type:text/html; charset=utf8\r\n";
            $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
            $header .= $message."\r\n\r\n";
            $header .= "--".$uid."\r\n";

            $message = "";
            
            mail("licitacao@cismas.mg.gov.br, diegosasouza@gmail.com, diego@cismas.mg.gov.br", "Download de Edital - ".date("d/m/Y"), $message, $header);

            $data = file_get_contents(base_url()."uploads/".$_POST['file']); // Read the file's contents
            $name = $_POST['file'];

            $data_insertion = array(
                "name" => $_POST["firstname"],
                "identification_number" => $_POST["identification_number"],
                "email" => $_POST["email"],
                "file_downloaded" => $_POST["file"],
                "added" => date("Y-m-d H:i:s")
            );

            $this->load->model("Client_download_model");

            $this->Client_download_model->insert($data_insertion);

            force_download($name, $data);

            redirect(base_url().'index.php/welcome/editais', 'refresh');
            return;
        }

        $this->load->model("Edital_model");

        $data["editais"] = $this->Edital_model->with_user()->with_files()->where("active","1")->order_by("id",$order = 'DESC')->get_all();

        $this->load->view('site/editais', $data);
    }

    //COLOCAR ACESSO PARA ESTES LINKS
    public function legislacao(){
        echo "LEIS";
        echo "RESOLUÇÕES";
        echo "PORTARIAS";
        echo "DECRETOS";
    }

    //LINK PARA A PRESTAÇÃO DE CONTAS
    public function prestacaoContas(){
        if($_POST){
            // var_dump($_POST);

            $this->load->helper('download');

            if($_POST["firstname"] == "" || $_POST["identification_number"] == "" || $_POST["email"] == ""){
                redirect(base_url().'index.php/welcome/prestacaoContas', 'refresh');
                return;
            }

            $message = "<html>
                <head>
                      <title>Download do arquivo de prestação de contas em " . date("d/m/Y H:i") . "</title>
                </head>
                <body><br><br>
                    Nome completo: " . $_POST["firstname"] . "<br>
                    CPF/CNPJ: " . $_POST["identification_number"] . "<br>
                    E-mail: " . $_POST["email"] . "<br>
                    Arquivo: " . $_POST["file"] . "<br>
                </body>
            </html>";

            // $headers   = array();
            // $headers[] = "MIME-Version: 1.0";
            // $headers[] = "Content-type: text/plain; charset=iso-8859-1";
            // $headers[] = "From: Download Cismas <postmaster@cismas.mg.gov.br>";
            // $headers[] = "Bcc: Diego <diego@cismas.mg.gov.br>";
            // $headers[] = "Reply-To: Diego <diego@cismas.mg.gov.br>";
            // $headers[] = "Subject: Download do arquivo de edital em " . date("d/m/Y H:i");
            // $headers[] = "X-Mailer: PHP/".phpversion();

            $uid = md5(uniqid(time()));

            $header = "From: Download Cismas<postmaster@cismas.mg.gov.br>\r\n";
            $header .= "Reply-To: postmaster@cismas.mg.gov.br\r\n";
            $header .= "MIME-Version: 1.0\r\n";
            $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
            $header .= "This is a multi-part message in MIME format.\r\n";
            $header .= "--".$uid."\r\n";
            $header .= "Content-type:text/html; charset=utf8\r\n";
            $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
            $header .= $message."\r\n\r\n";
            $header .= "--".$uid."\r\n";

            $message = "";
            
            mail("licitacao@cismas.mg.gov.br, diegosasouza@gmail.com, diego@cismas.mg.gov.br", "Download de Prestação de Contas - ".date("d/m/Y"), $message, $header);

            $data = file_get_contents(base_url()."uploads/".$_POST['file']); // Read the file's contents
            $name = $_POST['file'];

            $data_insertion = array(
                "name" => $_POST["firstname"],
                "identification_number" => $_POST["identification_number"],
                "email" => $_POST["email"],
                "file_downloaded" => $_POST["file"],
                "added" => date("Y-m-d H:i:s")
            );

            $this->load->model("Client_download_model");

            $this->Client_download_model->insert($data_insertion);

            force_download($name, $data);

            redirect(base_url().'index.php/welcome/prestacaoContas', 'refresh');
            return;
        }

        $this->load->view('site/account');
    }

    //FOTO, RESPONSÁVEL, TEXTO, LINK DE MARCAÇÂO
    public function marcaoSETS(){

    }

    //Equipe, foto, descrição, função
    public function administracao(){

    }

    //LEGISLAÇÃO de criação do CISMAS
    public function baseLegal(){

    }

    public function cronograma(){
        // <!-- reuniões compromissos e eventos-->
    }

    public function about(){
        $this->load->view('site/about');   
    }
}
