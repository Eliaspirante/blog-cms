<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_controller extends CI_Controller{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('admin/login/login');
    }

    public function updatepassword(){
        if($_POST){
            $data = array(
                'password' => $_POST["register-password"],
                'change_password' => 0
            );
            if($this->ion_auth->update($this->ion_auth->user()->row()->id, $data)){
                redirect('/welcome', 'refresh');
            }else
                $this->load->view('user/update_password');
        }else{
            $this->load->view('user/update_password');
        }
    }

    public function login(){
        if($_POST) {
            if ($this->ion_auth->login($this->input->post('login-user'), $this->input->post('login-password'))) {
//            if($this->ion_auth->user()->row()->change_password){
//                redirect('/login/updatepassword', 'refresh');
//                return;
//            }

                //if the login is successful
                //redirect them back to the home page
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect('admin/index', 'refresh');
            } else {
                //if the login was un-successful
                //redirect them back to the login page
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('login', 'refresh');
            }
        }
        $this->load->view('admin/login/login');
    }

    public function logout(){
        $this->ion_auth->logout();

        $this->session->unset_userdata(array('logged' => '0'));

        redirect('/', 'refresh');
    }
}