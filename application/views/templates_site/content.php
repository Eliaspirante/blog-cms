<!-- content -->
<div class="container">
    <div class="col-md-9">
        <?php if(isset($news) && $news != false):?>
            <?php foreach($news as $new):?>
                <section class="box-shadow bg-white">
                    <div class="col-md-12">
                        <h1 class="blue"><?php echo $new->title; ?><span class="gray"> - adicionado por: <?php echo $new->user->first_name; ?> em <?php echo date('d/m/Y H:i', strtotime($new->created_at));?></span></h1>
                    </div>
                    <?php if(isset($new->file_destaque) && $new->file_destaque != ""): ?>
                    <div class="col-md-4">
                        <img src="<?php echo base_url().'articles/'.$new->file_destaque; ?>" alt="" style="width: 263px;height: 200px;">
                    </div>
                    <div class="col-md-8">
                        <p><?php echo $new->description; ?></p>
                        <a href="<?php echo base_url(); ?>index.php/noticias/<?php echo $new->id; ?>" class="pull-right btn-more bg-blue white transition">Veja mais</a>
                    </div>
                    <?php else: ?>
                        <div class="col-md-12">
                            <p><?php echo $new->description; ?></p>
                            <a href="<?php echo base_url(); ?>index.php/noticias/<?php echo $new->id; ?>" class="pull-right btn-more bg-blue white transition">Veja mais</a>
                        </div>
                    <?php endif; ?>

                </section>
            <?php endforeach; ?>
        <?php else: ?>
            <section class="box-shadow bg-white">
                <div class="col-md-12">
                    <h1 class="blue">Nenhum notícia encontrada</h1>
                </div>
            </section>
        <?php endif; ?>
        <section class="box-shadow bg-white" style="margin-top: 151px;position: static;">
            <div class="title main white bg-blue">Parceiros</div>
            <div id="logosCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <a href="//www.saude.mg.gov.br/" target="_blank"><img src="<?php echo base_url();?>assets/img/slider_footer/MinasGeraisMARCA-1.jpg" style="width:200px;height:80px;margin-left: auto;margin-right: auto;"></a>
                    </div>
                    <div class="item">
                        <a href="//www.cosecsmg.org.br/portal/" target="_blank"><img src="<?php echo base_url();?>assets/img/slider_footer/logo_cosesc.jpg" style="width:200px;height:80px;margin-left: auto;margin-right: auto;"></a>
                    </div>
                    <div class="item">
                        <a href="//www.cosemsmg.org.br/" target="_blank"><img src="<?php echo base_url();?>assets/img/slider_footer/logo_cosems.png" style="width:200px;height:80px;margin-left: auto;margin-right: auto;"></a>
                    </div>
                    <div class="item">
                        <a href="//www.sets.saude.mg.gov.br/" target="_blank"><img src="<?php echo base_url();?>assets/img/slider_footer/sets.jpg" style="width:260px;height:80px;margin-left: auto;margin-right: auto;"></a>
                    </div>
                    <div class="item">
                        <a href="//portalsaude.saude.gov.br/" target="_blank"><img src="<?php echo base_url();?>assets/img/slider_footer/sus.jpg" style="width:200px;height:80px;margin-left: auto;margin-right: auto;"></a>
                    </div>
                </div>
                <a class="left carousel-control" href="#logosCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left light-blue" aria-hidden="true"></span>
                    <span class="sr-only">Anterior</span>
                </a>
                <a class="right carousel-control" href="#logosCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right light-blue" aria-hidden="true"></span>
                    <span class="sr-only">Próximo</span>
                </a>
            </div>
        </section>
    </div>