<!-- slider -->
<div class="container">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="<?php echo base_url();?>assets/img/slider/cismas_agradecimento.jpg" alt="Agradecimento" style="width:660px;height:420px;margin-left: auto;margin-right: auto;">
            </div>
            <div class="item">
                <img src="<?php echo base_url();?>assets/img/slider/cismas_frente.jpg" alt="Reforma no CISMAS" style="width:660px;height:420px;margin-left: auto;margin-right: auto;">
            </div>
        </div>
    </div>
</div>
<!--/.slider -->