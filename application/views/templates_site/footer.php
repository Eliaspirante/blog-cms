<!-- footer -->
<div class="row bg-blue">
    <div class="container padding-20">
        <div class="col-md-8">
            <div class="col-md-4">
                <ul class="footer-menu dark-blue">Mapa do Site
                    <li><a href="<?php echo base_url();?>">Home</a></li>
                    <li><a href="<?php echo base_url();?>index.php/sobre">Sobre Nós</a></li>
                    <li><a href="#">CISMAS</a></li>
                    <li><a href="#">Serviços</a></li>
                    <li><a href="#">Consorciados</a></li>
                    <li><a href="<?php echo base_url();?>index.php/noticias">Notícias</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="footer-menu dark-blue">Diário Oficial
                    <li><a href="//www.mgcidades.com.br/index.php?option=com_contpubl&idorg=259&tpform=1" target="_blank">Portal da Transparência</a></li>
                    <li><a href="#">Contratos e Convênios</a></li>
                    <li><a href="#">Legislação</a></li>
                    <li><a href="<?php echo base_url();?>index.php/editais">Editais</a></li>
                    <li><a href="#">Prestação de Contas</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="footer-menu dark-blue">Social
                    <li><i class="fa fa-facebook"></i><a href="https://www.facebook.com/Consórcio-Intermunicipal-de-Saúde-da-Microrregião-do-Alto-do-Sapucaí-424137031071419/">Facebook</a></li>
<!--                    <li><i class="fa fa-twitter"></i><a href="#">Twitter</a></li>-->
<!--                    <li><i class="fa fa-linkedin"></i><a href="#">LinkedIn</a></li>-->
<!--                    <li><i class="fa fa-rss"></i><a href="#">RSS</a></li>-->
                </ul>
            </div>
        </div>
        <div class="col-md-4">
            <form action="#" class="footer-form">
                <span class="footer-menu dark-blue">Fale Conosco</span>
                <textarea name="" id="" cols="40" rows="5" class="bg-light-blue" placeholder="Digite sua mensagem"></textarea>
                <input class="btn-more white bg-dark-blue pull-right transition" value="Enviar" type="submit">
            </form>
        </div>
        <div class="col-md-12 text-center asenine white adress">(35) 3622-1007    |    Rua Antônio Simão Mauad, 301, Centro     |    Itajubá - MG    |    37500-180    |    Brasil</div>
    </div>
</div>
<div class="container">
    <footer>
    </footer><!-- /.footer -->
</div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>