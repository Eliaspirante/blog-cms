<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="Site do Consórcio Intermunicipal de Saúde dos Municípios da Microrregião do Alto Sapucaí">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cismas - Consórcio Intermunicipal de Saúde dos Municípios da Microrregião do Alto Sapucaí</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="icon" type="image/x-icon" href="<?php echo base_url();?>assets/img/favicon.png">
    <link rel="author" href="<?php echo base_url();?>assets/authors.txt">

    <meta name="google-site-verification" content="EZcqde5syBlc3XUiHtG5r8qYd7qoR2RamGD-X8kFTcY" />

    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-53647915-5', 'auto');
	  ga('send', 'pageview');

	</script>
</head>
<body>