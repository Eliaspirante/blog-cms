<!-- header -->
<header class="container">
    <div class="col-md-12">
        <div class="logo bg-blue text-center col-md-3">
            <a href="#"><img src="<?php echo base_url();?>assets/img/logo-cismas.png" alt="Cismas"></a>
        </div>
        <div class="slogan bg-white col-md-9">
            <h1><p>Bem Vindo ao site</p>Consórcio Intermunicipal de Saúde dos Municípios da Microrregião do Alto Sapucaí</h1>
        </div>
    </div>
</header><!-- ./header -->