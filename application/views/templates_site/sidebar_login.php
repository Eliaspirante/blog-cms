<!-- sidebar -->
<div class="col-md-3">
    <section class="box-shadow bg-white">
        <div class="title white bg-blue">Área de Acesso</div>
        <form action="<?php echo base_url();?>index.php/login" method="post" class="form-login">
            <input class="col-md-12" type="text" name="login-user" placeholder="login">
            <input class="col-md-12" type="password" name="login-password" placeholder="senha">
            <a href="#" class="note">Esqueci minha senha</a>
            <input class="btn-more white bg-blue pull-right transition" value="OK" type="submit">
        </form>
    </section>
    <section class="box-shadow bg-white">
        <div class="title white bg-blue">Diário Oficial</div>
        <a href="//www.mgcidades.com.br/index.php?option=com_contpubl&idorg=259&tpform=1" target="_blank">
            <div class="col-md-12 docs transition">
                <img src="<?php echo base_url();?>assets/img/icon-bank.png" alt="" class="icon">
                <span>Portal da Transparência</span>
            </div>
        </a>
        <a href="#">
            <div class="col-md-12 docs transition">
                <img src="<?php echo base_url();?>assets/img/icon-files.png" alt="" class="icon">
                <span>Contratos e Convênios</span>
            </div>
        </a>
        <a href="#" target="_blank">
            <div class="col-md-12 docs transition">
                <img src="<?php echo base_url();?>assets/img/icon-gov.png" alt="" class="icon">
                <span>Legislação</span>
            </div>
        </a>
        <a href="<?php echo base_url();?>index.php/editais">
            <div class="col-md-12 docs transition">
                <img src="<?php echo base_url();?>assets/img/icon-chart.png" alt="" class="icon">
                <span>Editais e Licitações</span>
            </div>
        </a>
        <a href="<?php echo base_url();?>index.php/prestacaocontas">
            <div class="col-md-12 docs transition">
                <img src="<?php echo base_url();?>assets/img/icon-money.png" alt="" class="icon">
                <span>Prestação de Contas</span>
            </div>
        </a>
    </section>
</div>
</div>