<div class="container">
    <nav class="navbar navbar-default bg-white">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav nav-justified">
                    <li class="active"><a href="<?php echo base_url();?>">Home</a></li>
                    <li><a href="<?php echo base_url();?>index.php/sobre">Sobre Nós</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CISMAS <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Administração</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Base Legal</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Galeria</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Cronograma</a></li> 
                            <li role="separator" class="divider"></li>
                            <!-- <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Galeria <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Fotos</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#">Presidentes</a></li>                                    
                                </ul>
                            </li> -->
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Serviços <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="//sistema.cismas.mg.gov.br" target="_blank">Sistema de Marcação</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Transporte SETS</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Protocolos Assistênciais</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Relatórios</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Consorciados <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Municípios</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Mapa da Microrregião</a></li>
                            <li role="separator" class="divider"></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo base_url();?>index.php/noticias">Notícias</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>
</div><!--/.menu