<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>CISMAS - Consórcio Intermunicipal de Saúde dos Municípios da Microregião do Alto Sapucaí</title>

	<style type="text/css">
        IMG.displayed {
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 400px;
            height: 600px;
        }

        .center {
            margin: auto;
            text-align: center;
            padding: 10px;
        }
    
	</style>
</head>
<body>

<IMG class="displayed" src="<?php echo base_url();?>assets/img/cismas.jpg" alt="CISMAS - Consórcio Intermunicipal de Saúde dos Municípios da Microrregião do Alto Sapucaí">

<div class="center">
    <p>Nosso site está em construção, enquanto isso, acesse nossos editais pelo link abaixo.</p>
</div>

<div class="center">
<a href="<?php echo base_url();?>index.php/welcome/editais">Editais</a>
</div>
<!-- <table width="100%" height="100%" align="center" valign="center">
<tr><td>
<img src="<?php echo base_url();?>assets/img/cismas.jpg" width="250" height="400"/>
</td></tr>
</table> -->


</body>
</html>