<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <?php $this->load->view("templates/header") ?>
</head>
<body class="page-login" init-ripples="">
<div class="center">
    <div class="card bordered z-depth-2" style="margin:0% auto; max-width:400px;">
        <div class="card-header">
            <div class="brand-logo">
                <img src="<?php echo base_url();?>assets/img/favicon.png" /></div>
        </div>
        <div class="card-content">
            <div class="m-b-30">
                <div class="card-title strong blue-text">Login</div>
                <p class="card-title-desc"> Bem vindo ao painel administrativo do CISMAS</p>
            </div>
            <form action="<?php echo base_url();?>index.php/login" method="post" class="form-floating">
                <div class="form-group">
                    <label for="inputEmail" class="control-label">Usuário</label>
                    <input type="text" class="form-control" name="login-user"> </div>
                <div class="form-group">
                    <label for="inputPassword" class="control-label">Senha</label>
                    <input type="password" class="form-control" name="login-password"> </div>
<!--                <div class="form-group">-->
<!--                    <div class="checkbox">-->
<!--                        <label>-->
<!--                            <input type="checkbox"> Remember me </label>-->
<!--                    </div>-->
<!--                </div>-->
                <div class="card-action clearfix">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-link black-text">Login</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $this->load->view("templates/footer"); ?>
</body>
</html>