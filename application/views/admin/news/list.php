<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view("templates/header") ?>
</head>
<body scroll-spy="" id="top" class=" theme-template-light theme-blue alert-open alert-with-mat-grow-top-right">
<main>
    <aside class="sidebar fixed" style="width: 260px; left: 0px; ">
        <?php $this->load->view("templates/side_bar") ?>
    </aside>
    <div class="main-container">
       <?php $this->load->view("templates/container_header"); ?>
        <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="">
            <section class="tables-data">
                <div class="page-header">
                    <h1>      <i class="md md-list"></i>      Notícias    </h1>
                    <p class="lead">Listagem e alterações de notícias.</p>
                </div>
                <div class="card">
                    <div>
                        <div class="datatables">
                            <table id="example" class="table table-full table-full-small" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Título</th>
                                    <th>Descrição</th>
                                    <th>Criado por</th>
                                    <th>Criado em</th>
                                    <th>Ações</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Título</th>
                                    <th>Descrição</th>
                                    <th>Criado por</th>
                                    <th>Criado em</th>
                                    <th>Ações</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php if(isset($news) && $news != false): ?>
                                    <?php foreach($news as $new): ?>
                                        <tr>
                                            <td><?php echo $new->title; ?></td>
                                            <td><?php echo $new->description; ?></td>
                                            <td><?php echo $new->user->first_name; ?></td>
                                            <td><?php echo date('d/m/Y H:i', strtotime($new->created_at)); ?></td>
                                            <td><a style="color: white;" href="<?php echo base_url();?>index.php/dashboard/news/edit/<?php echo $new->id; ?>" class="btn btn-sm blue base btn-round"> <i class="md md-edit"></i> <div class="ripple-wrapper"></div></a></td>
                                            <td><a style="color: white;" href="#" onclick="deleteNew('<?php echo $new->id; ?>')" class="btn btn-sm red btn-round"> <i class="md md-delete"></i> <div class="ripple-wrapper"></div></a></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        Nenhum registro encontrado
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</main>
<?php $this->load->view("templates/footer"); ?>
<script>
    function deleteNew(id){
        r = confirm("Deseja realmente excluir está notícia?");

        if(r == true){
            $.ajax({
                method: "POST",
                url: "<?php echo base_url();?>index.php/dashboard/news/delete/",
                data: {article: id}
            }).done(function (msg) {
                var msg = JSON.parse(msg);
                if (msg.ok != undefined) {
                    alert(msg.ok);

                }else{
                    alert(msg.nok);
                }
            });
        }
    }
</script>
</body>
</html>