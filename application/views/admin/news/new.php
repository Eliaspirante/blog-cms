<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view("templates/header") ?>
</head>
<body scroll-spy="" id="top" class=" theme-template-light theme-blue alert-open alert-with-mat-grow-top-right">
<main>
    <aside class="sidebar fixed" style="width: 260px; left: 0px; ">
        <?php $this->load->view("templates/side_bar") ?>
    </aside>
    <div class="main-container">
        <?php $this->load->view("templates/container_header"); ?>
        <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="">
            <div class="col-md-12">
                <div class="well white">
                    <form actio="<?php echo base_url();?>index.php/admin/news/new" method="post" enctype="multipart/form-data" class="form-floating" onsubmit="getContent()">
                        <input type="hidden" name="content" id="content" />
                        <section class="tables-data">
                            <div class="page-header">
                                <h1>      <i class="md md-list"></i>      Cadastro de Notícia    </h1>
                            </div>
                            <div class="card">
                                <fieldset>
                                <div class="form-group filled">
                                    <label for="title" class="control-label">Título</label>
                                    <input type="text" class="form-control" value="" name="title" required> </div>
                                <div class="form-group filled">
                                    <label for="description" class="control-label">Resumo</label>
                                    <input type="text" class="form-control" value="" name="description" required> </div>
                                <div class="form-group filled">
                                    <label class="control-label">Imagem destaque</label>
                                    <input type="file" name="file_destaque" accept="image/*"> </div>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label class="filled">
                                            <input type="checkbox" checked="checked" name="active"> Notícia ativa? </label>
                                    </div>
                                </div>
                                <div class="row m-b-40" id="wysiwyg">
                                    <div class="col-md-12">
                                        <div class="well white">
                                                <fieldset>
                                                    <legend>Conteúdo da notícia</legend>
                                                    <div class="wysiwyg"></div>
                                                </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Salvar</button>
                                    <button type="reset" class="btn btn-default">Cancelar</button>
                                </div>
                                </fieldset>
                            </div>
                        </section>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
<?php $this->load->view("templates/footer"); ?>
<script>
    function getContent(){
        $("#content").val($(".note-editable").html());
    }
</script>
</body>
</html>