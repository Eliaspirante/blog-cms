<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view("templates/header") ?>
</head>
<body scroll-spy="" id="top" class=" theme-template-light theme-blue alert-open alert-with-mat-grow-top-right">
<main>
    <aside class="sidebar fixed" style="width: 260px; left: 0px; ">
        <?php $this->load->view("templates/side_bar") ?>
    </aside>
    <div class="main-container">
        <?php $this->load->view("templates/container") ?>
    </div>
</main>
<?php $this->load->view("templates/footer"); ?>
</body>
</html>