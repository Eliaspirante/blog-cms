<?php
$this->load->view('templates_site/header');
?>
    <div class="container-fluid">
        <?php
        $this->load->view('templates_site/header_div');
        $this->load->view('templates_site/menu');
?>
        <!-- content -->
        <div class="container">
            <div class="col-md-12">
                <section class="box-shadow bg-white">
                    <div class="col-md-12">
                        <h1 class="blue"><?php echo $new->title; ?><span class="gray"> - adicionado por: <?php echo $new->user->first_name; ?></span></h1>
                    </div>
                    <?php if(isset($new->file_destaque) && $new->file_destaque != ""): ?>
                        <div class="col-md-4">
                            <img src="<?php base_url();?>/articles/<?php echo $new->file_destaque; ?>" alt="" style="width:500px;margin-left: auto;margin-right: auto;">
                        </div>
                    <?php endif; ?>

                    <div class="col-md-12">
                        <p><?php echo $new->content; ?></p>
                    </div>
                </section>
            </div>
        <?php
        ?>
    </div>
<?php
$this->load->view('templates_site/footer');
?>