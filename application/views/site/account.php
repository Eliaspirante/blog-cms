<?php
$this->load->view('templates_site/header');
?>
    <div class="container-fluid">
<?php
$this->load->view('templates_site/header_div');
$this->load->view('templates_site/menu');
?>
    <!-- content -->
    <div class="container">
        <div class="col-md-9">
            <div class="box-shadow bg-white">
                <div class="col-md-9">
                    <div class="col-md-12">
                        <h3 class="blue">EXTRATO DE CONTRATOS<span class="gray"> - adicionado por: Cismas</span></h3>
                    </div>
                    <a href="#" onclick="chamaModal('EXTRATOS_DE_CONTRATOS-CISMAS.pdf');" class="btn-more white bg-blue pull-right transition">Baixar</a>
                </div>
            </div>
        </div>

        <div id="temp" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Formulário de preenchimento obrigatório</h4> </div>
                    <div class="modal-body">
                        <form action="<?php echo base_url();?>index.php/welcome/prestacaoContas" method="post" class="form-horizontal form-bordered" onsubmit="validaDados();">
                            <input type="hidden" id="file" name="file" value="">

                            <p>
                                <label for="identification_number">CPF/CNPJ: </label>
                                <input type="text" id="identification_number" name="identification_number" class="form-control" placeholder="" onblur="validar(this)" required>
                            </p>
                            <p>
                                <label for="email">E-mail: </label>
                                <input type="email" id="email" name="email" class="form-control" placeholder="" onblur="validateEmail(this)" required>
                            </p>
                            <p>
                                <label for="firstname">Nome: </label>
                                <input type="text" id="firstname" name="firstname" class="form-control" placeholder="" required>
                            </p>
                            <input type="submit" value="Enviar e baixar arquivo"/>
                            <input type="reset" value="Cancelar" onclick="closeModal();"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <script charset="utf-8" src="//maps.google.com/maps/api/js?sensor=false"></script>
        <script charset="utf-8" src="<?php echo base_url();?>assets/js/vendors.min.js"></script>
        <script charset="utf-8" src="<?php echo base_url();?>assets/js/app.min.js"></script>

        <script>
            function chamaModal(file){
                $("#temp").show();
                $('input[name="file"').val(file);
            }

            function closeModal(){
                $("#temp").hide();
            }
        </script>
        <?php
        $this->load->view('templates_site/sidebar_login');
        ?>
    </div>
    <script src="<?php echo base_url();?>assets/js/cpf_cnpj.js"></script>
<?php
$this->load->view('templates_site/footer');
?>