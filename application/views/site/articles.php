<?php
$this->load->view('templates_site/header');
?>
    <div class="container-fluid">
<?php
$this->load->view('templates_site/header_div');
$this->load->view('templates_site/menu');
$this->load->view('templates_site/slider');
?>
    <!-- content -->
    <!-- content -->
    <div class="container">
        <div class="col-md-9">
            <?php if(isset($news) && $news != false):?>
                <?php foreach($news as $new):?>
                    <section class="box-shadow bg-white">
                        <div class="col-md-12">
                            <h1 class="blue"><?php echo $new->title; ?><span class="gray"> - adicionado por: <?php echo $new->user->first_name; ?></span></h1>
                        </div>
                        <?php if(isset($new->file_destaque) && $new->file_destaque != ""): ?>
                            <div class="col-md-4">
                                <img src="<?php echo base_url().'articles/'.$new->file_destaque; ?>" alt="" style="width: 263px;height: 200px;">
                            </div>
                            <div class="col-md-8">
                                <p><?php echo $new->description; ?></p>
                                <a href="<?php echo base_url(); ?>index.php/noticias/<?php echo $new->id; ?>" class="pull-right btn-more bg-blue white transition">Veja mais</a>
                            </div>
                        <?php else: ?>
                            <div class="col-md-12">
                                <p><?php echo $new->description; ?></p>
                                <a href="<?php echo base_url(); ?>index.php/noticias/<?php echo $new->id; ?>" class="pull-right btn-more bg-blue white transition">Veja mais</a>
                            </div>
                        <?php endif; ?>

                    </section>
                <?php endforeach; ?>
            <?php else: ?>
                <section class="box-shadow bg-white">
                    <div class="col-md-12">
                        <h1 class="blue">Nenhum notícia encontrada</h1>
                    </div>
                </section>
            <?php endif; ?>
            <!--        <section class="box-shadow bg-white">-->
            <!--            <div class="title main white bg-blue">Parceiros</div>-->
            <!--            <div id="logosCarousel" class="carousel slide" data-ride="carousel">-->
            <!--                <div class="carousel-inner" role="listbox">-->
            <!--                    <div class="item active">-->
            <!--                        <img src="images/partners-01.jpg">-->
            <!--                    </div>-->
            <!--                    <div class="item">-->
            <!--                        <img src="images/partners-01.jpg">-->
            <!--                    </div>-->
            <!--                    <div class="item">-->
            <!--                        <img src="images/partners-01.jpg">-->
            <!--                    </div>-->
            <!--                </div>-->

            <!--                <a class="left carousel-control" href="#logosCarousel" role="button" data-slide="prev">-->
            <!--                    <span class="glyphicon glyphicon-chevron-left light-blue" aria-hidden="true"></span>-->
            <!--                    <span class="sr-only">Anterior</span>-->
            <!--                </a>-->
            <!--                <a class="right carousel-control" href="#logosCarousel" role="button" data-slide="next">-->
            <!--                    <span class="glyphicon glyphicon-chevron-right light-blue" aria-hidden="true"></span>-->
            <!--                    <span class="sr-only">Próximo</span>-->
            <!--                </a>-->
            <!--            </div>-->
            <!--        </section>-->
        </div>
        <?php
            $this->load->view('templates_site/sidebar_login')
        ?>
    </div>
<?php
$this->load->view('templates_site/footer');
?>