<?php
$this->load->view('templates_site/header');
?>
    <div class="container-fluid">
<?php
$this->load->view('templates_site/header_div');
$this->load->view('templates_site/menu');
?>
    <!-- content -->
    <!-- content -->
    <div class="container">
        <div class="col-md-9">
            <section class="box-shadow bg-white">
                <section class="box-shadow bg-white">
                    <div class="col-md-12">
                        <h1 class="blue">Sobre o CISMAS<span class="gray"></span></h1>
                    </div>
                    
                    <div class="col-md-12">
                        <img src="<?php echo base_url();?>assets/img/favicon.png" alt="" style="width: 263px;height: 200px;margin-left: 230px;margin-right: auto;">
                    </div>
                    <div class="col-md-12">
                        <p>
                        O Consórcio Intermunicipal de Saúde dos Municípios da Microrregião do Alto Sapucaí (CISMAS), com sede em Itajubá (MG), foi criado em 18 de Janeiro de 1996 com o intuito de desenvolvimento da microrregião. 
                        O objetivo do CISMAS na promoção a saúde são as consultas e exames de média complexidade e transporte em saúde seguro e gratuito (SETS) para todos os usuários dos municípios consorciados.
                        </p>
                        <p>
                        O CISMAS atualmente possui 14 municípios consorciados, com abrangência de uma população de aproximadamente 200.000 mil habitantes sendo eles: Brazópolis, Conceição das Pedras, Consolação, Delfim Moreira, Gonçalves, Itajubá, Maria da Fé, Marmelópolis, Pedralva, Piranguçu, Piranguinho, São José do Alegre, Sapucaí Mirim e Wenceslau Braz.
                        </p>
                        <p>
                        Nossa Missão é proporcionar um atendimento eficaz e de qualidade seguindo as diretrizes do SUS (Sistema Único de Saúde) em pareceria com as Prefeituras Municipais consorciadas através das Secretárias de Saúde.
                        </p>
                        <p>
                        O consórcio também possui vários serviços licitados, credenciados e conveniados dentro dos procedimentos de média complexidade. Hoje ofertamos aproximadamente 710 procedimentos, sanando o vazio assistencial atendendo as necessidade dos gestores e comunidade. 
                        </p>
                        <p>
                        Com isso o CISMAS se faz essencial no vinculo, proximidade e eficácia aos municípios, que necessita de uma instituição idônea, legal, pública e parceira do Estado e União para prestação dos serviços.
                        </p>
                    </div>
            </section>
        </div>
        <?php
            $this->load->view('templates_site/sidebar_login')
        ?>
    </div>
<?php
$this->load->view('templates_site/footer');
?>