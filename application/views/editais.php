<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>CISMAS - Consórcio Intermunicipal de Saúde dos Municípios da Microregião do Alto Sapucaí</title>
    <link href="<?php echo base_url();?>assets/css/vendors.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/css/styles.min.css" rel="stylesheet" />
    <script charset="utf-8" src="//maps.google.com/maps/api/js?sensor=true"></script>

    <script src="<?php echo base_url();?>assets/js/cpf_cnpj.js"></script>
</head>
<body>

<main>

    <div class="main-container">

        <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="">

            <a href="<?php echo base_url();?>"><- Voltar a página principal</a>

            <br/>
            <br/>
            <br/>
            <br/>

            Editais disponíveis

            <ul>
                <li>
                    <a href="#" onclick="chamaModal('EDITAL_PREGAO_PEÇAS.pdf');">EDITAL PREGÃO PEÇAS</a>
                </li>
                <li>
                    <a href="#" onclick="chamaModal('EDITAL_DE_IMPRESSORA_A_CERA.pdf');">EDITAL DE IMPRESSORA A CERA</a>
                </li>
                <li>
                    <a href="#" onclick="chamaModal('EDITAL_PREGAO_EXAMES.pdf');">EDITAL PREGÃO EXAMES</a>
                </li>
            </ul>

            <div id="temp" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Formulário de preenchimento obrigatório</h4> </div>
                        <div class="modal-body">
                            <form action="<?php echo base_url();?>index.php/welcome/editais" method="post" class="form-horizontal form-bordered" onsubmit="validaDados();">
                                <input type="hidden" id="file" name="file" value="">

                                <p>
                                    <label for="identification_number">CPF/CNPJ: </label>
                                    <input type="text" id="identification_number" name="identification_number" class="form-control" placeholder="" onblur="validar(this)" required>
                                </p>
                                <p>
                                    <label for="email">E-mail: </label>
                                    <input type="email" id="email" name="email" class="form-control" placeholder="" onblur="validateEmail(this)" required>
                                </p>
                                <p>
                                    <label for="firstname">Nome: </label>
                                    <input type="text" id="firstname" name="firstname" class="form-control" placeholder="" required>
                                </p>
                                <input type="submit" value="Enviar e baixar arquivo"/>
                                <input type="reset" value="Cancelar" onclick="closeModal();"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <script charset="utf-8" src="<?php echo base_url();?>assets/js/vendors.min.js"></script>
            <script charset="utf-8" src="<?php echo base_url();?>assets/js/app.min.js"></script>

            <script>
                function chamaModal(file){
                    $("#temp").show();
                    $('input[name="file"').val(file);
                }

                function closeModal(){
                    $("#temp").hide();
                }
            </script>
            </div>
        </div>
    </main>
</body>
</html>