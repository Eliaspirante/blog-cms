<?php
    $this->load->view('templates_site/header');
?>
<div class="container-fluid">
    <?php
    $this->load->view('templates_site/header_div');
    $this->load->view('templates_site/menu');
    $this->load->view('templates_site/slider');
    $this->load->view('templates_site/content');
    $this->load->view('templates_site/sidebar_login');
    ?>
</div>
<?php
    $this->load->view('templates_site/footer');
?>