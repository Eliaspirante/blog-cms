<?php
$group_user = 'members';
$group_edital = 'editals';
?>
<div class="brand-logo">
    <div id="logo">
            <img src="<?php echo base_url();?>assets/img/favicon.png" style="width:50px;height:50px;"/>

<!--        <div class="foot1"></div>-->
<!--        <div class="foot2"></div>-->
<!--        <div class="foot3"></div>-->
<!--        <div class="foot4"></div>-->
    </div>  </div>
<div class="user-logged-in">
    <div class="content">
        <div class="user-name"><?php echo $this->ion_auth->user()->row()->first_name; ?> <span class="text-muted f9"><?php echo $this->ion_auth->user()->row()->username; ?></span></div>
        <div class="user-email"><?php echo $this->ion_auth->user()->row()->email; ?></div>
        <div class="user-actions"><a href="<?php echo base_url();?>index.php/logout">sair</a> </div>
    </div>
</div>
<ul class="menu-links">
    <li icon="md md-blur-on"> <a href="<?php echo base_url();?>index.php/admin/"><i class="md md-blur-on"></i>&nbsp;<span>Principal</span></a></li>
    <li> <a href="#" data-toggle="collapse" data-target="#UIelements" aria-expanded="false" aria-controls="UIelements" class="collapsible-header waves-effect"><i class="md md-photo"></i>&nbsp;Notícias</a>
        <ul id="UIelements" class="collapse">
            <li> <a href="<?php echo base_url();?>index.php/dashboard/news/new"><span>Cadastrar</span></a></li>
            <li> <a href="<?php echo base_url();?>index.php/dashboard/news/"><span>Listagem</span></a></li>
        </ul>
    </li>
    <li> <a href="#" data-toggle="collapse" data-target="#Forms" aria-expanded="false" aria-controls="Forms" class="collapsible-header waves-effect"><i class="md md-input"></i>&nbsp;Páginas</a>
        <ul id="Forms" class="collapse">
            <li> <a href="#"><span>Sobre</span></a></li>
            <li> <a href="#"><span>Contato</span></a></li>
        </ul>
    </li>
</ul>