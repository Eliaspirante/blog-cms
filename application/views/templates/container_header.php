<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header pull-left">
            <button type="button" class="navbar-toggle pull-left m-15" data-activates=".sidebar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <ul class="breadcrumb">
                <li><a href="#">CMS</a></li>
                <li class="active">Área gerencial</li>
            </ul>
        </div>
    </div>
</nav>
