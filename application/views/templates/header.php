<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="CMS">
    <meta name="author" content="Diego Souza">
    <meta name="msapplication-TileColor" content="#9f00a7">
    <meta name="msapplication-TileImage" content="<?php echo base_url();?>assets/img/favicon/mstile-144x144.png">
    <meta name="msapplication-config" content="<?php echo base_url();?>assets/img/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <link rel="manifest" href="<?php echo base_url();?>assets/img/favicon/manifest.json">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/favicon.png">
    <title>Cismas - Sistema</title>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>  <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>  <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>  <![endif]-->
    <link href="<?php echo base_url();?>assets/css/vendors.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/css/styles.min.css" rel="stylesheet" />
    <script charset="utf-8" src="//maps.google.com/maps/api/js?sensor=false"></script>
</head>