
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Cismas - Consórcio Intermunicipal de Saúde dos Municípios da Microregião do Alto Sapucaí</title>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
        <link rel="icon" type="image/x-icon" href="<?php echo base_url();?>assets/img/favicon.png">
        <link rel="author" href="<?php echo base_url();?>assets/authors.txt">
    </head>
    <body>
        <div class="container-fluid">
        <!-- header -->
            <header class="container">
                <div class="col-md-12">
                    <div class="logo bg-blue text-center col-md-3">
                        <a href="#"><img src="<?php echo base_url();?>assets/img/logo-cismas.png" alt="Cismas"></a>
                    </div>
                    <div class="slogan bg-white col-md-9">
                        <h1><p>Bem Vindo ao site</p>Consórcio Intermunicipal de Saúde dos Municípios da Microregião do Alto Sapucaí</h1>
                    </div>
                </div>
            </header><!-- ./header -->
            <!-- menu -->
            <div class="container">
                <nav class="navbar navbar-default bg-white">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav nav-justified">
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#">Sobre Nós</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Consórcios <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Link 1</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#">Link 2</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#">Link 3</a></li>
                              </ul>
                            </li>
                            <li><a href="#">Serviços</a></li>
                            <li><a href="#">Diário Oficial</a></li>
                            <li><a href="#">Notícias</a></li>
                        </ul>
                    </div><!--/.nav-collapse -->
                    </div><!--/.container-fluid -->
                </nav>
            </div><!--/.menu -->
            <!-- slider -->
            <div class="container">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="images/slide_01.png" alt="Slide 01">
                        </div>
                        <div class="item">
                            <img src="images/slide_01.png" alt="Slide 01">
                        </div>
                        <div class="item">
                            <img src="images/slide_01.png" alt="Slide 01">
                        </div>
                    </div>
                </div>
            </div><!--/.slider -->
            <!-- content -->
            <div class="container">
                <div class="col-md-9">
                    <section class="box-shadow bg-white">
                        <div class="col-md-12">
                            <h1 class="blue">Headline 1<span class="gray"> - Lorem Ipsum</span></h1>
                        </div>
                        <div class="col-md-4">
                            <img src="images/thumb-01.jpg" alt="Notícia 1">
                        </div>
                        <div class="col-md-8">
                            <p>Nulla ac odio velit. Fusce fringilla hendrerit lorem, ac aliquet nunc adipiscing vel. Fusce justo nunc, consequat vel facilisis at, imperdiet at metus. Sed blandit tortor semper justo porttitor tempus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce posuere tempor justo luctus gravida. Morbi vehicula urna vel quam viverra iaculis. Pellentesque vitae pretium metus.</p>
                            <a href="#" class="pull-right btn-more bg-blue white transition">Veja mais</a>
                        </div>
                    </section>
                    <section class="box-shadow bg-white">
                        <div class="col-md-12">
                            <h1 class="blue">Headline 2<span class="gray"> - Lorem Ipsum</span></h1>
                        </div>
                        <div class="col-md-4">
                            <img src="images/thumb-02.jpg" alt="Notícia 2">
                        </div>
                        <div class="col-md-8">
                           <p>Nulla ac odio velit. Fusce fringilla hendrerit lorem, ac aliquet nunc adipiscing vel. Fusce justo nunc, consequat vel facilisis at, imperdiet at metus. Sed blandit tortor semper justo porttitor tempus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce posuere tempor justo luctus gravida. Morbi vehicula urna vel quam viverra iaculis. Pellentesque vitae pretium metus.</p>
                            <a href="#" class="pull-right btn-more bg-blue white transition">Veja mais</a>
                        </div>
                    </section>
                    <section class="box-shadow bg-white">
                        <div class="title main white bg-blue">Parceiros</div>
                        <div id="logosCarousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <img src="images/partners-01.jpg">
                                </div>
                                <div class="item">
                                    <img src="images/partners-01.jpg">
                                </div>
                                <div class="item">
                                    <img src="images/partners-01.jpg">
                                </div>
                            </div>
                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#logosCarousel" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left light-blue" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#logosCarousel" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right light-blue" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </section>
                </div>
                <!-- sidebar -->
                <div class="col-md-3">
                     <section class="box-shadow bg-white">
                        <div class="title white bg-blue">Área de Acesso</div>
                        <form action="#" class="form-login">
                            <input class="col-md-12" type="text" placeholder="login">
                            <input class="col-md-12" type="text" placeholder="senha">
                            <a href="#" class="note">Esqueci minha senha</a>
                            <input class="btn-more white bg-blue pull-right transition" value="OK" type="submit">
                        </form>
                    </section>
                    <section class="box-shadow bg-white">
                        <div class="title white bg-blue">Transparência</div>
                        <a href="#">
                            <div class="col-md-12 docs transition">
                               <img src="<?php echo base_url();?>assets/img/icon-files.png" alt="" class="icon">
                               <span>Contratos e Convênios</span>
                            </div>
                        </a>
                        <a href="#">
                            <div class="col-md-12 docs transition">
                               <img src="<?php echo base_url();?>assets/img/icon-bank.png" alt="" class="icon">
                               <span>Portal da Transparência</span>
                            </div>
                        </a>
                        <a href="#">
                            <div class="col-md-12 docs transition">
                               <img src="<?php echo base_url();?>assets/img/icon-money.png" alt="" class="icon">
                               <span>Nota Fiscal Eletrônica</span>
                            </div>
                        </a>
                        <a href="#">
                            <div class="col-md-12 docs transition">
                               <img src="<?php echo base_url();?>assets/img/icon-chart.png" alt="" class="icon">
                               <span>Editais e Licitações</span>
                            </div>
                        </a>
                        <a href="#">
                            <div class="col-md-12 docs transition">
                               <img src="<?php echo base_url();?>assets/img/icon-gov.png" alt="" class="icon">
                               <span>Secretarias do Governo</span>
                            </div>
                        </a>
                        <a href="#">
                            <div class="col-md-12 docs transition">
                               <img src="<?php echo base_url();?>assets/img/icon-academy.png" alt="" class="icon">
                               <span>Conselhos de Escolas</span>
                            </div>
                        </a>
                    </section>
                </div>
            </div>
            <!-- footer -->
            <div class="row bg-blue">
                <div class="container padding-20">
                    <div class="col-md-8">
                        <div class="col-md-4">
                            <ul class="footer-menu dark-blue">Mapa do Site
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Sobre Nós</a></li>
                                <li><a href="#">Consórcios</a></li>
                                <li><a href="#">Serviços</a></li>
                                <li><a href="#">Diário Oficial</a></li>
                                <li><a href="#">Notícias</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <ul class="footer-menu dark-blue">Transparência
                                <li><a href="#">Contratos</a></li>
                                <li><a href="#">Portal</a></li>
                                <li><a href="#">Nota Fiscal</a></li>
                                <li><a href="#">Editais</a></li>
                                <li><a href="#">Secretarias</a></li>
                                <li><a href="#">Conselhos</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <ul class="footer-menu dark-blue">Social
                                <li><i class="fa fa-facebook"></i><a href="#">Facebook</a></li>
                                <li><i class="fa fa-twitter"></i><a href="#">Twitter</a></li>
                                <li><i class="fa fa-linkedin"></i><a href="#">LinkedIn</a></li>
                                <li><i class="fa fa-rss"></i><a href="#">RSS</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <form action="#" class="footer-form">
                            <span class="footer-menu dark-blue">Fale Conosco</span>
                            <textarea name="" id="" cols="40" rows="5" class="bg-light-blue">Digite sua mensagem</textarea>
                            <input class="btn-more white bg-dark-blue pull-right transition" value="Enviar" type="submit">
                        </form>
                    </div>
                    <div class="col-md-12 text-center asenine white adress">+55 (11) 2242-2256    |    R. Joaquim Lopes Figueira, 101 - Mooca    |    São Paulo - SP    |    03176-080    |    Brasil</div>
                </div>
            </div>
            <div class="container">
                <footer>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi urna diam, molestie eu gravida vitae, tincidunt dignissim nunc. Proin nec mauris enim. Cras tempor efficitur felis, interdum consequat lacus scelerisque ut. Nam suscipit ligula vestibulum lacus feugiat lobortis eu sit amet lorem. Nunc vitae malesuada lectus, a pulvinar sem. Pellentesque facilisis turpis quis enim viverra lobortis. Maecenas tincidunt odio sed eros iaculis tempus. Proin suscipit commodo nisi quis molestie. Donec in volutpat orci, ut porttitor sapien.</p>
                </footer><!-- /.footer -->
           </div>
        </div>
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
</html>